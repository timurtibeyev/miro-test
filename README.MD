# Test REST application for Miro company

### Technologies

- Java 11
- Maven 3.2.5
- Swagger 2
- Checkstyle 8.32

### Verification

To run tests and syntax validation:
```
$ mvn clean verify
```

##### Checkstyle

To validate code using Checkstyle:
```
$ mvn checkstyle:checkstyle
```
Report folder `target/site/checkstyle`, output also will be printed in console.

##### Jacoco

To run tests and generate coverage report:
```
$ mvn test jacoco:report
```
Report folder `target/site/jacoco`.

### Installation

To start Spring Boot application:
```
$ mvn spring-boot:run
```

Url for CRUD operations - [http://localhost:8080/widgets](http://localhost:8080/widgets)

SwaggerUI url - [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)