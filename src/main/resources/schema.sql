create table if not exists widgets
(
    id         bigserial not null
    constraint widgets_pkey
    primary key,
    x_coordinate integer,
    y_coordinate integer,
    z_index integer,
    width integer,
    height integer,
    last_modification_date timestamp
);