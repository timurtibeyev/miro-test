package kz.tim.samples.mirotest.services;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import kz.tim.samples.mirotest.dto.CreateRequest;
import kz.tim.samples.mirotest.dto.UpdateRequest;
import kz.tim.samples.mirotest.entities.Widget;
import kz.tim.samples.mirotest.repositories.WidgetRepository;
import kz.tim.samples.mirotest.services.interfaces.WidgetService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link WidgetService} with {@code H2} database.
 *
 * @author Timur Tibeyev.
 */
@Service
public class WidgetServiceSqlImpl implements WidgetService {

    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private Lock readLock = lock.readLock();

    private final WidgetRepository widgetRepository;

    public WidgetServiceSqlImpl(WidgetRepository widgetRepository) {
        this.widgetRepository = widgetRepository;
    }

    @Override
    public Widget getWidgetById(Long id) {
        try {
            readLock.lock();
            return widgetRepository.findById(id).orElse(null);
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public boolean widgetExists(Long id) {
        try {
            readLock.lock();
            return widgetRepository.findById(id).isPresent();
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public Page<Widget> getWidgets(
            Integer page,
            Integer size,
            Integer minX,
            Integer minY,
            Integer maxX,
            Integer maxY
    ) {
        try {
            readLock.lock();
            final PageRequest pageRequest = PageRequest.of(page, size);
            if (minX != null && minY != null && maxX != null && maxY != null) {
                return widgetRepository.findAllInRegion(minX, maxX, minY, maxY, pageRequest);
            } else {
                return widgetRepository.findAll(pageRequest);
            }
        } finally {
            readLock.unlock();
        }
    }

    @Override
    @Transactional
    public Widget createWidget(CreateRequest createRequest) {
        try {
            writeLock.lock();
            final Widget newWidget = new Widget();
            newWidget.setxCoordinate(createRequest.getxCoordinate());
            newWidget.setyCoordinate(createRequest.getyCoordinate());
            newWidget.setWidth(createRequest.getWidth());
            newWidget.setHeight(createRequest.getHeight());

            if (createRequest.getzIndex() == null) {
                final Integer currentMaxzIndex = widgetRepository.findMaxzIndex();
                newWidget.setzIndex(currentMaxzIndex + 1);
            } else {
                widgetRepository.shiftUpwards(createRequest.getzIndex());
                newWidget.setzIndex(createRequest.getzIndex());
            }

            return widgetRepository.save(newWidget);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    @Transactional
    public Widget updateWidget(Long id, UpdateRequest updateRequest) {
        try {
            writeLock.lock();
            final Optional<Widget> optional = widgetRepository.findById(id);
            if (optional.isPresent()) {
                final Widget widget = optional.get();
                applyUpdates(widget, updateRequest);
                return widgetRepository.save(widget);
            } else {
                return null;
            }
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    @Transactional
    public boolean deleteWidget(Long id) {
        try {
            writeLock.lock();
            final Optional<Widget> optional = widgetRepository.findById(id);
            if (optional.isPresent()) {
                final Widget widget = optional.get();
                widgetRepository.shiftDownwards(widget.getzIndex());
                widgetRepository.deleteById(id);
                return true;
            } else {
                return false;
            }
        } finally {
            writeLock.unlock();
        }
    }

    private void applyUpdates(Widget widget, UpdateRequest updateRequest) {
        if (updateRequest.getxCoordinate() != null) {
            widget.setxCoordinate(updateRequest.getxCoordinate());
        }
        if (updateRequest.getyCoordinate() != null) {
            widget.setyCoordinate(updateRequest.getyCoordinate());
        }
        if (updateRequest.getWidth() != null) {
            widget.setWidth(updateRequest.getWidth());
        }
        if (updateRequest.getHeight() != null) {
            widget.setHeight(updateRequest.getHeight());
        }

        if (updateRequest.getzIndex() != null
                && !widget.getzIndex().equals(updateRequest.getzIndex())) {
            final Integer currentMaxzIndex = widgetRepository.findMaxzIndex();
            updateRequest.setzIndex(Math.min(updateRequest.getzIndex(), currentMaxzIndex));
            if (updateRequest.getzIndex() > widget.getzIndex()) {
                widgetRepository.shiftDownwards(widget.getzIndex(), updateRequest.getzIndex());
            } else {
                widgetRepository.shiftUpwards(updateRequest.getzIndex(), widget.getzIndex());
            }
            widget.setzIndex(updateRequest.getzIndex());
        }

        widget.setLastModificationDate(Instant.now());
    }
}
