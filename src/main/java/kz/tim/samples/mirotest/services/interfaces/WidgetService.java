package kz.tim.samples.mirotest.services.interfaces;

import kz.tim.samples.mirotest.dto.CreateRequest;
import kz.tim.samples.mirotest.dto.UpdateRequest;
import kz.tim.samples.mirotest.entities.Widget;
import org.springframework.data.domain.Page;

/**
 * Interface class for operations over widgets.
 *
 * @author Timur Tibeyev.
 */
public interface WidgetService {
    Widget getWidgetById(Long id);

    boolean widgetExists(Long id);

    Page<Widget> getWidgets(
            Integer page,
            Integer size,
            Integer minX,
            Integer minY,
            Integer maxX,
            Integer maxY
    );

    Widget createWidget(CreateRequest createRequest);

    Widget updateWidget(Long id, UpdateRequest updateRequest);

    boolean deleteWidget(Long id);
}
