package kz.tim.samples.mirotest.services;

import java.time.Instant;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import kz.tim.samples.mirotest.dto.CreateRequest;
import kz.tim.samples.mirotest.dto.UpdateRequest;
import kz.tim.samples.mirotest.entities.Widget;
import kz.tim.samples.mirotest.repositories.LocalRepository;
import kz.tim.samples.mirotest.services.interfaces.WidgetService;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link WidgetService} with local storage.
 *
 * @author Timur Tibeyev.
 */
@Primary
@Service
public class WidgetServiceLocalImpl implements WidgetService {

    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private Lock readLock = lock.readLock();

    private final LocalRepository localRepository;

    public WidgetServiceLocalImpl(LocalRepository localRepository) {
        this.localRepository = localRepository;
    }

    @Override
    public Widget getWidgetById(Long id) {
        try {
            readLock.lock();
            return localRepository.findById(id);
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public boolean widgetExists(Long id) {
        try {
            readLock.lock();
            return localRepository.findById(id) != null;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public Page<Widget> getWidgets(
            Integer page,
            Integer size,
            Integer minX,
            Integer minY,
            Integer maxX,
            Integer maxY
    ) {
        try {
            readLock.lock();
            if (minX != null && minY != null && maxX != null && maxY != null) {
                return localRepository.findAllInRegion(page, size, minX, minY, maxX, maxY);
            } else {
                return localRepository.findAll(page, size);
            }

        } finally {
            readLock.unlock();
        }
    }

    @Override
    public Widget createWidget(CreateRequest createRequest) {
        try {
            writeLock.lock();
            final Widget newWidget = new Widget();
            newWidget.setxCoordinate(createRequest.getxCoordinate());
            newWidget.setyCoordinate(createRequest.getyCoordinate());
            newWidget.setWidth(createRequest.getWidth());
            newWidget.setHeight(createRequest.getHeight());

            if (createRequest.getzIndex() == null) {
                newWidget.setzIndex(localRepository.findMaxzIndex() + 1);
            } else {
                localRepository.shiftUpwards(createRequest.getzIndex());
                newWidget.setzIndex(createRequest.getzIndex());
            }
            return localRepository.save(newWidget);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public Widget updateWidget(Long id, UpdateRequest updateRequest) {
        try {
            writeLock.lock();
            final Widget widget = localRepository.findById(id);
            if (widget != null) {
                applyUpdates(widget, updateRequest);
                return localRepository.save(widget);
            } else {
                return null;
            }
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public boolean deleteWidget(Long id) {
        try {
            writeLock.lock();
            final Widget widget = localRepository.findById(id);
            if (widget != null) {
                localRepository.shiftDownwards(widget.getzIndex());
                localRepository.deleteById(widget.getId());
                return true;
            } else {
                return false;
            }
        } finally {
            writeLock.unlock();
        }
    }

    private void applyUpdates(Widget widget, UpdateRequest updateRequest) {
        if (updateRequest.getxCoordinate() != null) {
            widget.setxCoordinate(updateRequest.getxCoordinate());
        }
        if (updateRequest.getyCoordinate() != null) {
            widget.setyCoordinate(updateRequest.getyCoordinate());
        }
        if (updateRequest.getWidth() != null) {
            widget.setWidth(updateRequest.getWidth());
        }
        if (updateRequest.getHeight() != null) {
            widget.setHeight(updateRequest.getHeight());
        }

        if (updateRequest.getzIndex() != null
                && !widget.getzIndex().equals(updateRequest.getzIndex())) {
            final int currentMaxzIndex = localRepository.findMaxzIndex();
            updateRequest.setzIndex(Math.min(updateRequest.getzIndex(), currentMaxzIndex));
            if (updateRequest.getzIndex() > widget.getzIndex()) {
                localRepository.shiftDownwards(widget.getzIndex(), updateRequest.getzIndex());
            } else {
                localRepository.shiftUpwards(updateRequest.getzIndex(), widget.getzIndex());
            }
            widget.setzIndex(updateRequest.getzIndex());
        }

        widget.setLastModificationDate(Instant.now());
    }
}
