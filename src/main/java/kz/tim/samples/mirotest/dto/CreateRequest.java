package kz.tim.samples.mirotest.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Widget create/update request.
 *
 * @author Timur Tibeyev.
 */
public class CreateRequest {
    @NotNull
    private Integer xCoordinate;

    @NotNull
    private Integer yCoordinate;

    private Integer zIndex;

    @NotNull
    private Integer width;

    @NotNull
    private Integer height;

    public Integer getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(Integer xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Integer getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(Integer yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Integer getzIndex() {
        return zIndex;
    }

    public void setzIndex(Integer zIndex) {
        this.zIndex = zIndex;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
