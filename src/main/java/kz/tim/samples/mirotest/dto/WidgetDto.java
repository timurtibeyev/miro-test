package kz.tim.samples.mirotest.dto;

import java.time.Instant;
import kz.tim.samples.mirotest.entities.Widget;

/**
 * DTO class for {@link Widget}.
 *
 * @author Timur Tibeyev.
 */
public class WidgetDto {
    private Long id;

    private Integer xCoordinate;

    private Integer yCoordinate;

    private Integer zIndex;

    private Integer width;

    private Integer height;

    private Instant lastModificationDate;

    /**
     * Creates {@link WidgetDto} instance from corresponding {@link Widget}.
     *
     * @param widget {@link Widget} class
     */
    public WidgetDto(Widget widget) {
        this.id = widget.getId();
        this.xCoordinate = widget.getxCoordinate();
        this.yCoordinate = widget.getyCoordinate();
        this.zIndex = widget.getzIndex();
        this.width = widget.getWidth();
        this.height = widget.getHeight();
        this.lastModificationDate = widget.getLastModificationDate();
    }

    public WidgetDto() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(Integer xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Integer getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(Integer yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Integer getzIndex() {
        return zIndex;
    }

    public void setzIndex(Integer zIndex) {
        this.zIndex = zIndex;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Instant getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Instant lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }
}
