package kz.tim.samples.mirotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class MiroTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiroTestApplication.class, args);
    }

}
