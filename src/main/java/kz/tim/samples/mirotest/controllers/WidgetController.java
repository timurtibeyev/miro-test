package kz.tim.samples.mirotest.controllers;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import kz.tim.samples.mirotest.dto.CreateRequest;
import kz.tim.samples.mirotest.dto.UpdateRequest;
import kz.tim.samples.mirotest.dto.WidgetDto;
import kz.tim.samples.mirotest.entities.Widget;
import kz.tim.samples.mirotest.services.interfaces.WidgetService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class responsible for handling {@code CRUD} operations over widgets.
 *
 * @author Timur Tibeyev.
 */
@RestController
@RequestMapping("/widgets")
@Validated
public class WidgetController {

    private final Logger logger = LogManager.getLogger(getClass());

    private final WidgetService widgetService;

    public WidgetController(
            WidgetService widgetService
    ) {
        this.widgetService = widgetService;
    }

    /**
     * Endpoint for filtering widget list.
     *
     * @param page page number, default 0
     * @param size number of elements on page, default 10, maximum 500
     * @param minX left bound of the filtering region
     * @param minY lower bound of the filtering region
     * @param maxX right bound of the filtering region
     * @param maxY upper bound of the filtering region
     *
     * @return {@code 200} and filtered results if exists, {@code 404} if not found
     */
    @GetMapping
    public ResponseEntity getWidgets(
            @RequestParam(value = "page", defaultValue = "0") @Min(0) Integer page,
            @RequestParam(value = "size", defaultValue = "10") @Min(1) @Max(500) Integer size,
            @RequestParam(value = "minX", required = false) Integer minX,
            @RequestParam(value = "minY", required = false) Integer minY,
            @RequestParam(value = "maxX", required = false) Integer maxX,
            @RequestParam(value = "maxY", required = false) Integer maxY

    ) {
        logger.info("Get all widgets request");
        final Page<Widget> widgetPage = widgetService
                .getWidgets(page, size, minX, minY, maxX, maxY);
        final List<WidgetDto> dtos = widgetPage.map(WidgetDto::new).toList();
        return new ResponseEntity(dtos, HttpStatus.OK);
    }

    /**
     * Endpoint for retrieving widgets by id.
     *
     * @param id existing {@link Widget} id
     *
     * @return {@code 200} and widget description if exists, {@code 404} if not found
     */
    @GetMapping("/{id}")
    public ResponseEntity getWidget(@PathVariable(value = "id") Long id) {
        logger.info("Get widget by id request");
        final Widget widget = widgetService.getWidgetById(id);
        if (widget == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(new WidgetDto(widget), HttpStatus.OK);
        }
    }

    /**
     * Endpoint for creating widgets.
     *
     * @param createRequest {@link Widget} content
     *
     * @return {@code 201} and widget description if updated, {@code 404} if not found
     */
    @PostMapping
    public ResponseEntity createWidget(@Valid @RequestBody CreateRequest createRequest) {
        logger.info("Create new widget request");
        final Widget widget = widgetService.createWidget(createRequest);
        return new ResponseEntity(new WidgetDto(widget), HttpStatus.CREATED);
    }

    /**
     * Endpoint for updating widgets.
     *
     * @param id existing {@link Widget} id
     * @param updateRequest {@link Widget} content
     *
     * @return {@code 200} and widget description if updated, {@code 404} if not found
     */
    @PutMapping("/{id}")
    public ResponseEntity updateWidget(
            @PathVariable(value = "id") Long id,
            @Valid @RequestBody UpdateRequest updateRequest
    ) {
        logger.info("Update existing widget request");
        final Widget widget = widgetService.updateWidget(id, updateRequest);
        if (widget == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(new WidgetDto(widget), HttpStatus.OK);
        }
    }

    /**
     * Endpoint for deleting widgets.
     *
     * @param id {@link Widget} id
     *
     * @return {@code 204} if deleted, {@code 404} if not found
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteWidget(@PathVariable(value = "id") Long id) {
        logger.info("Delete widget request");
        if (widgetService.deleteWidget(id)) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
