package kz.tim.samples.mirotest.repositories;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import kz.tim.samples.mirotest.entities.Widget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

/**
 * DAO for {@link Widget} class. Local storage implementation.
 *
 * @author Timur Tibeyev.
 */
@Repository
public class LocalRepository {
    private HashMap<Long, Widget> map = new HashMap<>();
    private AtomicLong idGenerator = new AtomicLong(1);

    /**
     * Finds widget by id.
     *
     * @param id {@link Widget} id
     *
     * @return {@link Widget} object
     */
    public Widget findById(Long id) {
        return map.get(id);
    }

    /**
     * Shifts widgets upwards.
     *
     * @param minIndex minimum zIndex
     * @param maxIndex maximum zIndex
     */
    public void shiftUpwards(Integer minIndex, Integer maxIndex) {
        map.values().stream()
                .filter(w -> (w.getzIndex() >= minIndex && w.getzIndex() <= maxIndex))
                .forEach(w -> w.setzIndex(w.getzIndex() + 1));
    }

    /**
     * Shifts widgets upwards.
     *
     * @param zIndex minimum zIndex
     */
    public void shiftUpwards(Integer zIndex) {
        map.values().stream()
                .filter(w -> (w.getzIndex() >= zIndex))
                .forEach(w -> w.setzIndex(w.getzIndex() + 1));
    }

    /**
     * Shifts widgets downwards.
     *
     * @param minIndex minimum zIndex
     * @param maxIndex maximum zIndex
     */
    public void shiftDownwards(Integer minIndex, Integer maxIndex) {
        map.values().stream()
                .filter(w -> (w.getzIndex() >= minIndex && w.getzIndex() <= maxIndex))
                .forEach(w -> w.setzIndex(w.getzIndex() - 1));
    }

    /**
     * Shifts widgets downwards.
     *
     * @param zIndex minimum zIndex
     */
    public void shiftDownwards(Integer zIndex) {
        map.values().stream()
                .filter(w -> (w.getzIndex() >= zIndex))
                .forEach(w -> w.setzIndex(w.getzIndex() - 1));
    }

    /**
     * Finds maximum zIndex.
     *
     * @return maximum zIndex among all widgets
     */
    public int findMaxzIndex() {
        Optional<Widget> maxWidget = map.values().stream()
                .max((Comparator.comparingInt(Widget::getzIndex)));
        int zindex = 0;
        if (maxWidget.isPresent()) {
            zindex = maxWidget.get().getzIndex();
        }
        return zindex;
    }

    /**
     * Deletes widget by id.
     *
     * @param id {@link Widget} id
     */
    public void deleteById(Long id) {
        map.remove(id);
    }

    /**
     * Saves widget.
     *
     * @param widget {@link Widget} object
     *
     * @return saved {@link Widget} object
     */
    public Widget save(Widget widget) {
        if (widget.getId() == null) {
            widget.setId(idGenerator.getAndIncrement());
        }
        map.put(widget.getId(), widget);
        return widget;
    }

    /**
     * Finds widgets inside region.
     *
     * @param page result list page
     * @param size result list size
     * @param minX minimum X coordinate
     * @param minY minimum Y coordinate
     * @param maxX maximum X coordinate
     * @param maxY maximum Y coordinate
     *
     * @return paginated and filtered results
     */
    public Page<Widget> findAllInRegion(
            Integer page,
            Integer size,
            Integer minX,
            Integer minY,
            Integer maxX,
            Integer maxY
    ) {
        List<Widget> list = map.values().stream()
                .filter(w -> (isWidgetInsideRange(w, minX, minY, maxX, maxY)))
                .sorted(((o1, o2) -> (o2.getzIndex() - o1.getzIndex())))
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());
        return new PageImpl<>(list);
    }

    /**
     * Finds widgets.
     *
     * @param page result list page
     * @param size result list size
     *
     * @return paginated results
     */
    public Page<Widget> findAll(
            Integer page,
            Integer size
    ) {
        List<Widget> list = map.values().stream()
                .sorted(((o1, o2) -> (o2.getzIndex() - o1.getzIndex())))
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList());

        return new PageImpl<>(list);
    }

    /**
     * Deletes all widgets.
     */
    public void deleteAll() {
        map.clear();
    }

    private boolean isWidgetInsideRange(
            Widget widget,
            Integer minX,
            Integer minY,
            Integer maxX,
            Integer maxY
    ) {
        return widget != null
                && (widget.getxCoordinate() != null)
                && (widget.getyCoordinate() != null)
                && (widget.getWidth() != null)
                && (widget.getHeight() != null)
                && (widget.getxCoordinate() >= minX)
                && (widget.getxCoordinate() + widget.getWidth() <= maxX)
                && (widget.getyCoordinate() >= minY)
                && (widget.getyCoordinate() + widget.getHeight() <= maxY);
    }
}
