package kz.tim.samples.mirotest.repositories;

import kz.tim.samples.mirotest.entities.Widget;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * DAO for {@link Widget} class.
 *
 * @author Timur Tibeyev.
 */
@Repository
public interface WidgetRepository extends CrudRepository<Widget, Long>,
        PagingAndSortingRepository<Widget, Long> {

    @Query("Select coalesce(max(w.zIndex), 0) from Widget w")
    Integer findMaxzIndex();

    @Query("Select w from Widget w order by w.zIndex desc")
    Page<Widget> findAll(Pageable pageable);

    @Query("Select w from Widget w where w.xCoordinate >= ?1 and w.xCoordinate + w.width <= ?2"
            + " and w.yCoordinate >= ?3 and w.yCoordinate + w.height <= ?4 order by w.zIndex desc")
    Page<Widget> findAllInRegion(
            Integer minX,
            Integer maxX,
            Integer minY,
            Integer maxY,
            PageRequest pageRequest
    );

    @Modifying
    @Query("Update Widget w set w.zIndex = w.zIndex + 1 where w.zIndex >= ?1")
    void shiftUpwards(Integer zIndex);

    @Modifying
    @Query("Update Widget w set w.zIndex = w.zIndex + 1 where w.zIndex >= ?1 and w.zIndex <= ?2")
    void shiftUpwards(Integer fromIndex, Integer toIndex);

    @Modifying
    @Query("Update Widget w set w.zIndex = w.zIndex - 1 where w.zIndex >= ?1")
    void shiftDownwards(Integer zIndex);

    @Modifying
    @Query("Update Widget w set w.zIndex = w.zIndex - 1 where w.zIndex >= ?1 and w.zIndex <= ?2")
    void shiftDownwards(Integer fromIndex, Integer toIndex);
}
